//
//  Toast.h
//  multa-moral
//
//  Created by Felipe B. Valio on 5/9/13.
//  Copyright (c) 2013 Pinuts Studios. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Toast : UIView

+ (void) showWithMessage: (NSString*) message;

@end
