//
//  AppDelegate.h
//  toast
//
//  Created by Felipe B. Valio on 5/14/13.
//  Copyright (c) 2013 Felipe B. Valio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
