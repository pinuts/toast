//
//  Toast.m
//  multa-moral
//
//  Created by Felipe B. Valio on 5/9/13.
//  Copyright (c) 2013 Pinuts Studios. All rights reserved.
//

#import "Toast.h"
#import <QuartzCore/QuartzCore.h>

static const float MaxToastWidth = 200;
static const float MinToastWidth = 80;
static const float ToastMargin = 10;
static const float ToastDuration = 5;

@implementation Toast

+ (void) showWithMessage: (NSString*) message {
    
    Toast *toast = [self createToastWithMessage:message];
    
    UIView *screenView = [[UIApplication sharedApplication].delegate window].rootViewController.view;
    
    toast.center = CGPointMake(screenView.frame.size.width/2,
                               screenView.frame.size.height*2/3);
    
    [screenView addSubview:toast];
    [toast showAnimated];
}

+ (Toast*) createToastWithMessage: (NSString*) message {
    UILabel *label = [UILabel new];
    label.text = message;
    label.numberOfLines = 0;
    label.numberOfLines = 10;
    label.backgroundColor = [UIColor clearColor];
    label.textColor = [UIColor whiteColor];
    label.textAlignment = NSTextAlignmentCenter;
    label.font = [UIFont systemFontOfSize:14];
    
    Toast *toast = [Toast new];
    toast.backgroundColor = [UIColor colorWithWhite:0 alpha:0.7];
    toast.layer.cornerRadius = 10;
    [toast addSubview:label];

    CGRect rect = label.frame;
    rect.size = [label sizeThatFits:CGSizeMake(MaxToastWidth*8/10, 1000)];
    
    rect.size.width += 2*ToastMargin;
    rect.size.height += 2*ToastMargin;
    
    if( rect.size.width > MaxToastWidth ) {
        rect.size.width = MaxToastWidth;
    }
    
    if( rect.size.width < MinToastWidth ) {
        rect.size.width = MinToastWidth;
    }
    
    toast.frame = rect;
    
    rect.size.width -= 2*ToastMargin;
    rect.origin.x += ToastMargin;
    label.frame = rect;
    
    return toast;
}



- (void) showAnimated {
    self.alpha = 0;
    [UIView animateWithDuration:0.5
                     animations:^{
                         self.alpha = 1;
                     }
                     completion:^(BOOL finished) {
                         [self performSelector:@selector(dismissAnimated) withObject:nil afterDelay:ToastDuration];
                     }];
}


- (void) dismissAnimated {
    
    [UIView animateWithDuration:0.5
                     animations:^{
                         self.alpha = 0;
                     }
                     completion:^(BOOL finished) {
                         [self removeFromSuperview];
                     }];
}

@end
