//
//  ViewController.m
//  toast
//
//  Created by Felipe B. Valio on 5/14/13.
//  Copyright (c) 2013 Felipe B. Valio. All rights reserved.
//

#import "ViewController.h"
#import "Toast.h"

@interface ViewController ()

@end

@implementation ViewController

- (void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [Toast showWithMessage:@"bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla"];
}
@end
